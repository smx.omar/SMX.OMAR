# SSH X11 (CLIENTE) EN LINUX
**OMAR SMX**

En el servidor o maquina que utilicemos editar el archivo /etc/ssh/ssh_config.         
Tendremos que loguearnos como root "su -" y  poner la contraseña de super usuario seguramente..
editar el archivo /etc/ssh/ssh_config. 
  - Aremos un >**geany etc/ssh/ssh_config**
  - Posiblemente tengamos lo siguiente #"X11 Forwarding no"
  - Cambiaremos ese parametro por este #"X11 Forwarding yes"
  - Despues de este cambio, posiblemente sea necesario reiniciar el demonio ssh.
   - Usaremos: **systemctl restart sshd**
  

 
### Instalacion
Para ver el estado del demon:
```sh
$ systemctl status sshd
```


**En caso de que estuviera apagado, lo encenderemos con un:**

```sh
$ systemctl start sshd
```

Ahora tendremos que conectarnos a un servidor con el parametro **"X"** en el ssh
```sh
$ ssh -X root@192.168.X.X
```


Para **conexiones mas lentas** podemos usar el parametro **"C"** que es de "Compressed"
```sh
$ ssh -X -C root@192.168.X.X
```
### **Apuntes**
**Para que nos sirve esto?**
Imaginemos que tenemos un servidor en run level 3 (Command-Line) y queremos hacer un movimiento de particiones con G-Parted, cabe recordar que no tenemos modo grafico en el servidor, el X11 lo tiene que tener el cliente, no el servidor, esto significa que, podemos usar los programas sin tenerlos instalado.
 - **EL SERVIDOR** **EJECUTA EL PROGRAMA (/G-PARTED)**
  - **NOSOTROS DIBUJAMOS EL PROGRAMA (CON EL X11)**


### Docker
Dillinger is very easy to install and deploy in a Docker container.

By default, the Docker will expose port 80, so change this within the Dockerfile if necessary. When ready, simply use the Dockerfile to build the image.

```sh
cd dillinger
docker build -t <youruser>/dillinger:latest .
```
This will create the dillinger image and pull in the necessary dependencies. Once done, run the Docker and map the port to whatever you wish on your host. In this example, we simply map port 80 of the host to port 80 of the Docker (or whatever port was exposed in the Dockerfile):

```sh
docker run -d -p 80:80 --restart="always" <youruser>/dillinger:latest
```

Verify the deployment by navigating to your server address in your preferred browser.




