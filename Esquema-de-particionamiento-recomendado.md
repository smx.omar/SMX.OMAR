### 1. Esquema de particionamiento recomendado
### 1.2. Sistemas x86, AMD64 e Intel® 64

Recomendamos que cree las siguientes particiones para los sistemas x86, AMD4 e Intel®:
```sh
Una partición swap
Una partición /boot
Una partición /
```

Una partición **swap** (de al menos 256MB) — las particiones swap (o de intercambio) son utilizadas para dar soporte a la memoria virtual. En otras palabras, los datos son escritos a una partición swap cuando no hay suficiente memoria RAM para almacenar los datos que su sistema está procesando.
En años anteriores, la cantidad de espacio de intercambio (swap) recomendado se aumentaba linealmente con la cantidad de RAM del sistema. Pero debido a que la cantidad de memoria en sistemas modernos se aumentó a cientos de gigabytes, ahora se reconoce que la cantidad de espacio swap que un sistema necesita es una función de la carga de trabajo de la memoria del sistema. Sin embargo, dado a que el espacio de intercambio se define al momento de instalar, y a que es difícil determinar de antemano la cantidad de carga de trabajo en memoria de un sistema, se recomienda usar la siguiente tabla para determinar el espacio swap del sistema.


![Esquema recomendado](https://i.gyazo.com/e2d7f078328f71b617f6b0d3bde1b333.png)


### Una partición /boot (250 MB)
La partición montada en /boot/ contiene el kernel del sistema operativo (el cual permite a su sistema arrancar Fedora) junto con archivos utilizados durante el proceso de arranque(grub2,modulos,etc) . Para la mayoría de los usuarios, una partición boot de 250 MB es suficiente.
```sh
Btrfs:
El gestor de arranque GRUB no tiene soporte para un sistema de archivos Btrfs. No puede utilizar una partición btrfs para /boot.
```
```sh
Nota:
Si tiene una tarjeta RAID, tenga en cuenta que algunas BIOSes no soportan el arranque desde la tarjeta RAID. En tales casos, se debe crear la partición /boot/ en una partición fuera de la formación RAID, tal como en un disco duro separado.
```


### Una partición root (3.0 GB - 5.0 GB)
This is where "/" **(the root directory)** is located. In this setup, all files (except those stored in /boot) are on the root partition.
3.0 GB le permite instalar una instalación mínima, mientras que una partición raíz de 5.0 GB le permite realizar una instalación completa, seleccionando todos los grupos de paquetes.


```sh
Raíz y /root* o alreves
The / (or root) partition is the top of the directory structure. The /root directory (sometimes pronounced "slash-root") directory is the home directory of the user account for system administration.
```

La siguiente tabla resume los tamaños mínimos de partición para los directorios listados. Usted no tiene que crear una partición separada para cada uno de esos directorios.


![RECOMENDACIÓN](https://i.gyazo.com/7c0d25106608bfdbbcbd948dc6a00141.png)



**El directorio /usr guarda la mayoría del contenido del software en un sistema Fedora.** Para una instalación con el conjunto estandar de software, asigne al menos 4 GB de espacio. Si usted es un desarrollador de software o planea usar Fedora para aprender y desarrollar sus habilidades en ese sentido, debe considerar al menos doblar ese tamaño.
```sh
No poner /usr en una partición separada
Si /usr está en una partición separada de /, el proceso de inicialización se vuelve mucho más complejo, y en algunas situaciones (como instalaciones sobre unidades iSCSI), podría no funcionar.
```


Ejemplo de uso
**Esta configuración no es óptima para todos los casos de uso.**
![enter image description here](https://i.gyazo.com/0cc2f538d47f13c0bcfa9b3150ece086.png)
FIN