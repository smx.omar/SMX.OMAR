### Qué es Markdown?
Es un lenguaje que nos permite escribir texto con formato de manera sencilla y rápida, sin tener que abandonar nunca el teclado, en busca de ese botón para cambiar nuestro texto a negritas, a itálicas, o incluír un link. Es perfecto, pues, para simplemente escribir, manteniendo el texto formateado.
## MarkdownPad
```sh
http://markdownpad.com/
```
MarkdownPad nos ofrece siempre un “Live Preview”, o vista previa constante, para ver exactamente qué estamos haciendo, o cómo está funcionando – perfecto si recién están acostumbrándose a este sistema.

La aplicación también es bastante personalizable, e incluso podemos añadir botones (para poner el texto en negritas, itálicas) si no nos acostumbramos por completo (la idea, por supuesto, es terminar por deshabilitar esta barra en el futuro).
```sh
http://markdownpad.com/img/markdownpad2.png
```

Tenemos una gran variedad de temas, colores, fuentes para elegir, y algo sumamente importante: tras editar el texto en Markdown, podemos exportarlo rápidamente como HTML a nuestro clipboard, para luego pegarlo en nuestra plataforma favorita (Blogger, WordPress, lo que sea)

MarkdownPad también incluye un modo “Distraction-Free”, que están sumamente de moda. Tras presionar F10, MarkdownPad ocupará toda la pantalla, para así preocuparnos sólo por el texto que estamos escribiendo.

### Aplicaciones compatibles con Markdown
**Aplicaciones compatibles** con **Markdown** las hay de sobra. Las ventajas son tantas que la mayoría de los principales editores incorporan soporte. Además de las nuevas propuestas que aparecen, tanto para sistemas operativos de escritorio como para **dispositivos móviles**. A continuación os listo las que considero son las mejores opciones:

**IA Writer**: disponible para iOS y Mac. Un diseño minimalista es su princical atractivo. Luego, otras características como el cálculo de tiempo de lectura, palabras escritas o el modo sin distracciones hacen que resulta una gran opción. Y soporta la sincronización en iCloud.
```sh
http://img.blogs.es/anexom/wp-content/uploads/2014/02/nvalt.jpg
```

**nvALT 2**: Un port basado en Notational Velocity, sólo para Mac, resulta ser una aplicación con un gran potencial de la que me agrada mucho su buscador y la posibilidad de acceder a todos los textos creados con la app.
```sh
http://img.blogs.es/anexom/wp-content/uploads/2014/02/wm_sc1.jpg
```
**MOU**: similar a MarkdownPad sólo que para Mac. Su principal ventaja, la previsualización en la misma ventana.
```sh
http://img.blogs.es/anexom/wp-content/uploads/2014/02/promo-image.jpg
```
**UberWriter**: es un editor minimalista para Linux. Poco que decir, si usáis Linux debéis instalarla. Tiene modo a pantalla completa y Focus Mode para evitar distracciones.
```sh
http://img.blogs.es/anexom/wp-content/uploads/2014/02/retext-kde.jpg
```

 **HarooPad (Windows, Mac, Linux)**: 
 Probablemente, otro de los editores de Markdown más potentes sea HarooPad. Una de sus características más destacables es que posee una interfaz sencilla y muy amigable para usuarios con pocos conocimientos.

Incluye una guía rápida a modo de chuleta desplegable a la izquierda.
Procesa Markdown, GitHub Flavour Markdown (GFM) o permite personalizarlo.
Soporte completo de personalización de temas y estilos CSS.
Permite exportar en formato HTML.
Es gratuito, multiplataforma y tienes su código fuente disponible en **GitHub.**
```sh
http://i.emezeta.com/weblog/markdown-editores/haroopad.png
```
## Código en MarkDown Extra
En **MarkDown Extra** no tenemos que preocuparnos por indentar el código de bloque con cuatro espacios, puesto que nos ofrece otra alternativa, una línea antes y después del bloque utilizar al menos tres tildes ~~~. De esta manera, ademá podemos incluir líneas antes y después del código, sin que MarkDown las elimine.
```sh
~~~
**<?php**
* Aquí podemos poner cualquier código, si necesidad de indentarlo
*/
echo ('es más fácil');
~~~
```
##Tablas
MarkDown Extra, nos permite crear tablas sencillas. La sintaxis me parece muy acertada pues visualmente tenemeos una tabla, con lo que se apega a la filosofía de MarkDown.
```sh
| Columna 1     | Columna 2     |
| ------------- | ------------- |
| Celda 1, col1 | Celda 2, col2 |
| Celda 3, col1 | Celda 3, col2 |
```
La tabulación para que queden las columnas alineadas no es un requisito, tampoco lo es la pipa al inicio o al final de la cada fila, por lo que también esta tabla también sería válida.
```sh
Día | Ingresos | Egresos
--- | --- | ---
1 | $25000 | $50
2 | $200 | $320
3 | $5 | $50000
```
